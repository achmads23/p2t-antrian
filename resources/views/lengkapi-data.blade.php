@include('layouts.header-2')
<section class="section-padding-40 about-section-s5">
    <div class="container">
        @php
            if(Session::get('imk')){
                $imk = Session::get('imk');
                if($imk == 'online'){
                    $imk = 'Izin Online dari P2T';
                } else if($imk == 'oss'){
                    $imk = 'OSS';
                } else if($imk == 'biasa'){
                    $imk = 'Konsul izin Biasa';
                }
            } else {
                $imk = '';
            }
        @endphp
        <h5 class='text-center'>{{ strtoupper($jenis_layanan)}}</h5>
        @if ($imk!= NULL)
            <h5 class='text-center'>{{ strtoupper($imk)}}</h5>
        @endif
        <h4 class='text-center'>{{ $data->sektor->nama_kategori}}</h4>
        <h4 class='text-center'>{{ $data->perizinan->nama_sub_kategori}}</h4>
        <br>
        <a href="{{url($jenis_layanan . '/' . $data->sektor->id)}}" class='btn btn-default btn-lg'><i class="fa fa-chevron-left"></i> Kembali</a>
        <h2 class='text-center' style="margin-top:5px;">Lengkapi Data</h2>
        <br>
        <div class="row">
            <div class='col-xs-12'>
                <form method="POST" action="{{ url('/ajukan')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="url" value="{{ url()->current()}}">
                    <input type="hidden" name="jenis_layanan" value="{{ $jenis_layanan}}">
                    <input type="hidden" name="id_sektor" value="{{ $data->sektor->id}}">
                    <input type="hidden" name="imk" value="{{ $imk}}">
                    <input type="hidden" name="id_lokasi" value="{{ $data->tempat_layanan->id}}">
                    <input type="hidden" name="id_perizinan" value="{{ $data->perizinan->id}}">
                    @if ($jenis_layanan == 'perizinan')
                        <div class="form-group">
                            <label for="exampleInputEmail1">Jenis</label>
                            <select class="form-control" name="jenis" required>
                                <option value="Baru">Baru</option>
                                <option value="Perpanjangan">Perpanjangan</option>
                                <option value="Perubahan">Perubahan</option>
                            </select>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="exampleInputEmail1">Perorangan/Badan Usaha</label>
                        <select class="form-control badan-usaha" name="status" required>
                            <option value="Perorangan">Perorangan</option>
                            <option value="Badan usaha">Badan usaha</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1" id="nik-badan-usaha">NIK</label>
                        <input class="form-control" name="nik" placeholder="NIK" required id="nik-badan-usaha-input">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Pemohon</label>
                        <input class="form-control" name="nama" placeholder="Nama Pemohon" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alamat</label>
                        <input class="form-control" name="alamat" placeholder="Alamat" required>
                    </div>
                    <div class="form-group tanpa-badan-hukum">
                        <label for="exampleInputEmail1">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin">
                            <option value="Laki-Laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group tanpa-badan-hukum">
                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                        <input class="form-control datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input class="form-control" type="email" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nomor WA Handphone</label>
                        <input class="form-control" type="telp" name="telp" placeholder="Nomor WA Handphone" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Pendidikan</label>
                        <select class="form-control" name="pendidikan" required>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA/STM/SMK">SMA/STM/SMK</option>
                            <option value="D/S1">D/S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                            <option value="Lain-lain">Lain-lain</option>
                        </select>
                    </div>
                    @if ($jenis_layanan == 'konsultasi')
                        <div class="form-group">
                            <label for="exampleInputEmail1">Perihal</label>
                            <input class="form-control" type="text" name="perihal" placeholder="Perihal" required>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-2">
                            <a href="{{ url($jenis_layanan . '/' . $data->sektor->id)}}" class='btn btn-default' style="width:100%">Kembali</a>
                        </div>
                        <div class="col-md-10">
                            <button type="submit" class='btn btn-success' style="width:100%">Simpan dan Cetak Nomor Antrian</button>
                        </div>
                    </div>
                </form>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
    <!-- end container -->
</section>
<!-- end service-single-section -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/moment.js')}}"></script>
<script src="{{asset('plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

<script type="text/javascript">
    $('.datepicker').datetimepicker({
        format: 'DD-MM-Y',
    });

    $('.badan-usaha').on('change', function(event) {
        if($(this).val() == 'Badan usaha'){
            $('#nik-badan-usaha').html('NPWP');
            $('#nik-badan-usaha-input').attr("placeholder", "NPWP");
            $('.tanpa-badan-hukum').addClass('hidden');
        } else {
            $('.tanpa-badan-hukum').removeClass('hidden');
            $('#nik-badan-usaha').html('NIK');
            $('#nik-badan-usaha-input').attr("placeholder", "NIK");
        }
        /* Act on the event */
    });

    $('input[name="nik"]').on('keyup', function(event) {
        let val = $(this).val();
        if(val.length > 15){
            $(".loading").css('display', 'flex');
            $.ajax({
                url: '{{asset('/cek_nik')}}',
                type: 'POST',
                data: {nik: val,_token:'{{csrf_token()}}'},
            })
            .done(function(data) {
                try {
                    data = JSON.parse(data.data);
                    $('input[name="nama"]').val(data.content[0].NAMA_LGKP);
                    $('input[name="alamat"]').val(data.content[0].ALAMAT + ', ' + data.content[0].KEL_NAME + ', ' + data.content[0].KEC_NAME + ', ' + data.content[0].KAB_NAME + ', ' + data.content[0].PROP_NAME);
                    $('select[name="jenis_kelamin"]').val(data.content[0].JENIS_KLMIN).change();
                    var tgl_lahir = data.content[0].TGL_LHR.split('-');
                    $('input[name="tanggal_lahir"]').val(tgl_lahir[2] + '-' + tgl_lahir[1] + '-' + tgl_lahir[0]);
                } catch(e) {
                    alert('Data NIK Tidak Ditemukan')
                }
            })
            .fail(function() {
                alert('Data NIK Tidak Ditemukan')
            })
            .always(function() {
                console.log("complete");
                $(".loading").css('display', 'none');
            });
        }
    });
</script>