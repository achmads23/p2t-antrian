@include('layouts.header-2')
    <section class="section-padding-40 about-section-s5">
            <div class="container">
                @php
            if(Session::get('imk')){
                $imk = Session::get('imk');
                if($imk == 'online'){
                    $imk = 'Izin Online dari P2T';
                } else if($imk == 'oss'){
                    $imk = 'OSS';
                } else if($imk == 'biasa'){
                    $imk = 'Konsul izin Biasa';
                }
            } else {
                $imk = '';
            }
        @endphp
        <h5 class='text-center'>{{ strtoupper($jenis_layanan)}}</h5>
        @if ($imk!= NULL)
            <h5 class='text-center'>{{ strtoupper($imk)}}</h5>
        @endif
                <h2 class='text-center' style="margin-top:5px;">Pilih Sektor</h2>

                <a href="{{url('/')}}" class='btn btn-default btn-lg'><i class="fa fa-chevron-left"></i> Kembali</a>

                <div class="row">
                    @foreach ($data->sektor as $row)
                        <div class="col-xs-6 col-md-3 pb-20 pt-20">
                            <div class="card perizinan">
                                <a href="{{ url($jenis_layanan.'/'.$row->id)}}">
                                <img src="{{(Config::get('app.url_asset') . 'assets/upload/kategori/'.$row->gambar)}}">
                                <div class="card-box">
                                    <h5 class="text-white"><?= $row->nama_kategori?></h5>
                                </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- end container -->
        </section>
	</body>
</html>