@include('layouts.header-2')    
    <section class="section-padding-40 about-section-s5">
            <div class="container">
                @php
            if(Session::get('imk')){
                $imk = Session::get('imk');
                if($imk == 'online'){
                    $imk = 'Izin Online dari P2T';
                } else if($imk == 'oss'){
                    $imk = 'OSS';
                } else if($imk == 'biasa'){
                    $imk = 'Konsul izin Biasa';
                }
            } else {
                $imk = '';
            }
        @endphp
        <h5 class='text-center'>{{ strtoupper($jenis_layanan)}}</h5>
        @if ($imk!= NULL)
            <h5 class='text-center'>{{ strtoupper($imk)}}</h5>
        @endif
                <h4 class='text-center'>{{$data->sektor->nama_kategori}}</h4>
                <br>
                <a href="{{url($jenis_layanan)}}" class='btn btn-default btn-lg'><i class="fa fa-chevron-left"></i> Kembali</a>
                <h2 class='text-center' style="margin-top:5px;">Pilih Izin</h2>
                <br>
                <div class="table-responsive">
                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th data-hide="phone" style="text-align: center;">No</th>
                                <th data-class="expand"> Nama Sub-kategori</th>
                                <th data-hide="phone,tablet" style="width: 25%;text-align: center;"> Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data->perizinan as $key => $row)
                                <tr>
                                    <td width="5%">
                                        {{$key+1}}
                                    </td>
                                    <td>
                                        {{$row->nama_sub_kategori}}
                                    </td>
                                    <td width="5%" class="text-center">
                                        <a href="{{url($jenis_layanan.'/'.$data->sektor->id.'/'.$row->id)}}" class="btn btn-success">Pilih
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end container -->
        </section>
        <!-- end service-single-section -->

<script src="{{asset('/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js')}}"></script>
<script src="{{asset('/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>

<script src="{{asset('/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>
<script type="text/javascript">
    var responsiveHelper_dt_basic2 = undefined;
    var breakpointDefinition = {
            tablet : 1024,
            phone : 480
        };
    $(function($) {
        var t = $('#dt_basic').DataTable({
        // "order": [[ 3, "desc" ]]
        });

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw(); 
    });
</script>
</body>
</html>
