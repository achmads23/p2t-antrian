<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="P2T Provinsi Jawa timur">
    <!-- Page Title -->
    <title> Pelayanan Perizinan Terpadu | Provinsi Jawa Timur </title>
    <link href="{{asset('img/logo-jatim.ico')}}" rel="shortcut icon">
    <!-- Icon fonts -->
    <link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/flaticon.css')}}" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Plugins for this template -->
    <link href="{{asset('front/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.theme.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slick-theme.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.transitions.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/bootstrap-select.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/style.css')}}" rel="stylesheet">
</head>
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
        font-size: 11px;
    }

    .amcharts-pie-slice {
        transform: scale(1);
        transform-origin: 50% 50%;
        transition-duration: 0.3s;
        transition: all .3s ease-out;
        -webkit-transition: all .3s ease-out;
        -moz-transition: all .3s ease-out;
        -o-transition: all .3s ease-out;
        cursor: pointer;
        box-shadow: 0 0 30px 0 #000;
    }

    .amcharts-pie-slice:hover {
        transform: scale(1.1);
        filter: url(#shadow);
    }

    .center-img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    .info-box {
        text-align: center;
        margin-top: -45px;
        background: #fff;
        position: relative;
        padding: 20px;
        border-radius: 10px;
        background: #ffde15;
    }

    .info-box:after {
        clear: both;
        display: table;
        content: '';
    }

    .info-box a {
        font-size: 17px;
        float: right;
        text-transform: uppercase;
        font-weight: 600;
        border-radius: 6px;
        color: #fff;
        transition: all .5s ease;
        width: 100%;
    }

    .info-box h3 {
        float: left;
        margin-top: 10px;
        margin-bottom: 0;
    }

    .info-p2t{
        font-size: 20px;
        text-align: center;
        padding:30px;
    }

    @media(max-width:767px){
        .info-box a {
            float: none;
            display: inline-block;
            font-size: 14px;

        }
    }
    @media(min-width:768px){}
    @media(min-width:992px){}
    @media(min-width:1200px){}

    .page-wrapper{
        height: 100%;
    }

    .monitor-section{
        height: calc(100% - 79px);
    }

    .monitor-section .layanan{
        height:100%;
    }

    .box-pilih-layanan{
        background-image: url('{{asset('assets/images/kotak.png')}}');
        background-repeat: no-repeat;
        background-position: center;
        padding: 80px 0;
        display: block;
        /* padding: 20%; */
        font-size: 35px;
        font-weight: bold;
        /* width: 80%; */
        margin-bottom: 50px;
    }

    .box-pilih-layanan.center{
        text-align: center;
    }
    .box-pilih-layanan .lingkaran{
        background-image: url('{{asset('assets/images/label.png')}}');
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
        position: absolute;
        width: fit-content;
        padding: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        color: white;
    }

    .box-pilih-layanan .lingkaran.kiri{
        right: 22%;
        bottom: 5%;
    }

    .box-pilih-layanan .lingkaran.kanan{
        right: 22%;
        bottom: 5%;
    }
</style>

<body>
    <div class="page-wrapper">
    @php
    $datetime = DateTime::createFromFormat('d-m-Y', date('d-m-Y'));
    $days = [
        'Sunday' => 'Minggu',
        'Monday' => 'Senin',
        'Tuesday' => 'Selasa',
        'Wednesday' => 'Rabu',
        'Thursday' => 'Kamis',
        'Friday' => 'Jumat',
        'Saturday' => 'Sabtu',
    ];

    $months = [
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];
    $now = date('d-m-Y'); 
    $now = explode("-", $now);
    echo $days[$datetime->format('l')] . ', ' . (int)$now[0] . ' ' . $months[(int)$now[1]] . ' ' . $now[2];
    @endphp
    <nav class="navbar navbar-default navbar-fixed-top navbar-monitor">
        <div class="container">
            <div id="navbar" class="navbar-collapse collapse">
                <a class="navbar-brand" href="{{url('/')}}"><img style="width: 35px" src="{{asset('images/logo.png')}}" alt></a>
                <div class="scroll-left">
                    @if ($data->tempat_layanan)
                    <p><?php echo $data->tempat_layanan->running_text ?></p>
                    @else
                    <p>Selamat Datang di Service Point Pelayanan Perizinan Terpadu di {{$data->tempat_layanan->nama}}</p>
                    @endif
                </div>
                <div class="right">
                    {{$days[$datetime->format('l')]}}<br>
                    {{(int)$now[0] . ' ' . $months[(int)$now[1]] . ' ' . $now[2]}}
                </div>
            </div><!--/.nav-collapse -->
        </div>
    </nav>