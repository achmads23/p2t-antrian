<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="P2T Provinsi Jawa timur">
    <!-- Page Title -->
    <title> Pelayanan Perizinan Terpadu | Provinsi Jawa Timur </title>
    <link href="{{asset('img/logo-jatim.ico')}}" rel="shortcut icon">
    <!-- Icon fonts -->
    <link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/flaticon.css')}}" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Plugins for this template -->
    <link href="{{asset('front/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.theme.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slick-theme.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.transitions.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/bootstrap-select.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/style.css')}}" rel="stylesheet">
    <style type="text/css">
        .header-monitor{
            background:url({{asset('images/header/header-2.jpg')}});
            background-size: cover;
            padding:40px;
            border-bottom:5px solid #3cc7bc;
            background-position-y: -100px;
        }

        .loader {
          border: 16px solid #f3f3f3; /* Light grey */
          border-top: 16px solid #3498db; /* Blue */
          border-radius: 50%;
          width: 80px;
          height: 80px;
          animation: spin 2s linear infinite;
          margin-bottom: 10px;
        }

        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
        .loading{
            position: fixed;
            z-index: 9999999999;
            top: 0;
            left: 0;
            display: none;
            justify-content: center;
            align-items: center;
            background-color: rgb(0,0,0,0.7);
            width: 100%;
            height: 100%;
            flex-direction: column;
            color: white;
        }
    </style>
</head>
<body>
    <div class="loading">
    <div class="loader"></div>
    Mohon Tunggu
    </div>
    <div class="page-wrapper">
    <nav class="header-monitor">
        <img src="{{asset('img/logo-dpmptsp.png')}}" style="height:100px;">
    </nav>