
    <div class="footer" style="display: flex;width: 100%;padding:10px;background-color: #002957; color:white;font-size: 15px;position: fixed;bottom: 0;">
        <span id="running-text" style="    margin-right: 0px;margin-left: 10px;width: 70px;">12:15 | </span>
        <div class="running-text" style="width: 100%;overflow: hidden;">
            <div class="scroll-running-text">
                @if ($data->tempat_layanan)
                <p style="color:yellow !important;margin:0;line-height: 1.5;white-space: nowrap;"><?php echo $data->tempat_layanan->running_text ?></p>
                @else
                <p style="color:yellow !important;margin:0;line-height: 1.5;white-space: nowrap;">Selamat Datang di Service Point Pelayanan Perizinan Terpadu di {{$data->tempat_layanan->nama}}</p>
                @endif
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" integrity="sha256-H9jAz//QLkDOy/nzE9G4aYijQtkLt9FvGmdUTwBk6gs=" crossorigin="anonymous"></script>
            <script type="text/javascript">
                var intvalls = setInterval(function() {
                    var momentNow = moment();
                    $('#running-text').html(momentNow.format('hh:mm') + ' | ');
                }, 100);
</script>
</body>
</html>