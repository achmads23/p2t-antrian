@include('layouts.header-2')
<section class="section-padding-40 about-section-s5">
    <div class="container">
        <h5 class='text-center'>{{ strtoupper($jenis_layanan == 'legalisir' ? 'Pendampingan OSS' : 'Pengambilan dan Legalisir')}}</h5>
        <br>
        <h2 class='text-center' style="margin-top:5px;">Lengkapi Data</h2>
        <br>
        <div class="row">
            <div class='col-xs-12'>
                <form method="POST" action="{{ url('/antrian/' . $jenis_layanan)}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="url" value="{{ url()->current()}}">
                    <input type="hidden" name="jenis_layanan" value="{{ $jenis_layanan}}">
                    <input type="hidden" name="id_lokasi" value="{{ $data->tempat_layanan->id}}">
                    
                    <div class="form-group">
                        <label for="exampleInputEmail1" id="nik-badan-usaha">NIK</label>
                        <input class="form-control" name="nik" placeholder="NIK" required id="nik-badan-usaha-input">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Pemohon</label>
                        <input class="form-control" name="nama" placeholder="Nama Pemohon" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alamat</label>
                        <input class="form-control" name="alamat" placeholder="Alamat" required>
                    </div>
                    <div class="form-group tanpa-badan-hukum">
                        <label for="exampleInputEmail1">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin">
                            <option value="Laki-Laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group tanpa-badan-hukum">
                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                        <input class="form-control datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input class="form-control" type="email" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nomor WA Handphone</label>
                        <input class="form-control" type="telp" name="telp" placeholder="Nomor WA Handphone" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Pendidikan</label>
                        <select class="form-control" name="pendidikan" required>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA/STM/SMK">SMA/STM/SMK</option>
                            <option value="D/S1">D/S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                            <option value="Lain-lain">Lain-lain</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <a href="{{ url('/')}}" class='btn btn-default' style="width:100%">Kembali</a>
                        </div>
                        <div class="col-md-10">
                            <button type="submit" class='btn btn-success' style="width:100%">Simpan dan Cetak Nomor Antrian</button>
                        </div>
                    </div>
                </form>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
    <!-- end container -->
</section>
<!-- end service-single-section -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/moment.js')}}"></script>
<script src="{{asset('plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

<script type="text/javascript">
    $('.datepicker').datetimepicker({
        format: 'DD-MM-Y',
    });

    $('input[name="nik"]').on('keyup', function(event) {
        let val = $(this).val();
        if(val.length > 15){
            $(".loading").css('display', 'flex');
            $.ajax({
                url: '{{asset('/cek_nik')}}',
                type: 'POST',
                data: {nik: val,_token:'{{csrf_token()}}'},
            })
            .done(function(data) {
                try {
                    data = JSON.parse(data.data);
                    $('input[name="nama"]').val(data.content[0].NAMA_LGKP);
                    $('input[name="alamat"]').val(data.content[0].ALAMAT + ', ' + data.content[0].KEL_NAME + ', ' + data.content[0].KEC_NAME + ', ' + data.content[0].KAB_NAME + ', ' + data.content[0].PROP_NAME);
                    $('select[name="jenis_kelamin"]').val(data.content[0].JENIS_KLMIN).change();
                    var tgl_lahir = data.content[0].TGL_LHR.split('-');
                    $('input[name="tanggal_lahir"]').val(tgl_lahir[2] + '-' + tgl_lahir[1] + '-' + tgl_lahir[0]);
                } catch(e) {
                    alert('Data NIK Tidak Ditemukan')
                }
            })
            .fail(function() {
                alert('Data NIK Tidak Ditemukan')
            })
            .always(function() {
                console.log("complete");
                $(".loading").css('display', 'none');
            });
        }
    });
</script>