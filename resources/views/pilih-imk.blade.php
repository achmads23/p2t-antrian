@include('layouts.header-2')  
<section class="section-padding-40 about-section-s5">
    <div class="container">
        <h5 class='text-center'>{{ strtoupper($jenis_layanan)}}</h5>
        <h2 class='text-center' style="margin-top:5px;">Pilih IMK</h2>

        <a href="{{url('/')}}" class='btn btn-default btn-lg'><i class="fa fa-chevron-left"></i> Kembali</a>

        <div class="row hidden-xs">
            <div class="col-md-4 pb-20 pt-20">
                <div class="card perizinan">
                    <a href="{{url('pilih_imk/online')}}">
                        <img src="{{ asset('upload/kategori/pengairan.jpg')}}">
                        <div class="card-box">
                            <h5 class="text-white">Izin Online dari P2T</h5>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 pb-20 pt-20">
                <div class="card perizinan">
                    <a href="{{url('pilih_imk/oss')}}">
                        <img src="{{ asset('upload/kategori/pengairan.jpg')}}">
                        <div class="card-box">
                            <h5 class="text-white">OSS</h5>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 pb-20 pt-20">
                <div class="card perizinan">
                    <a href="{{url('pilih_imk/biasa')}}">
                        <img src="{{ asset('upload/kategori/pengairan.jpg')}}">
                        <div class="card-box">
                            <h5 class="text-white">Konsul izin Biasa</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
