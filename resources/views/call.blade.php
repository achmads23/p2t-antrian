<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Voice Antrian</title>
    <link href="{{asset('img/logo-jatim.ico')}}" rel="shortcut icon">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            flex-direction: column;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
    <div class="flex-center position-ref full-height">

        <div class="content">
            <div class="title m-b-md" id="call-zone">
                Antrian tidak ada
            </div>
        </div>
        <div class="text-center">
            <strong><span style="color:green">DPMPTSP</span> &copy; {{date('Y')}} </strong>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script type="text/javascript">
    var playedOnceFlag = false;

    function play(audio, callback) {
        playedOnceFlag = true;
        try {
            audio.play();
        } catch(e) {
            alert('Please just click ok.');
            return play(audio, callback);
        }

        if (callback) {
            audio.onended = callback;
        }
    }
    function queue_sounds(sounds){

        var index = 0;
        function recursive_play()
        {
            if(index+1 === sounds.length)
            {
                play(sounds[index],null);
                setTimeout(call, 3000);
            }
            else
            {
                play(sounds[index],function(){index++; recursive_play();});
            }
        }

        recursive_play();   
    }

    function call(){
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if (this.readyState != XMLHttpRequest.DONE) return;
            console.log('XMLHttpRequest.DONE');

            if (this.status >= 400) {
              // refresh if at least one successful call
              if (playedOnceFlag) {
                location.reload();
              } else {
                setTimeout(call, 3000);
              }
            }

            if (this.status == 200) {
                let result = JSON.parse(xhttp.responseText);

                var call_zone = document.getElementById("call-zone");
                if(result.code == 400){
                    setTimeout(call, 2000);
                    call_zone.innerHTML = 'Antrian tidak ada';
                } else {
                    call_zone.innerHTML = result.data.antrian + ' di Meja ' + result.data.nama_meja;

                    var arr = [];
                    $.each(result.data.array_panggilan, function(index, val) {
                        var audio = new Audio("{{URL::to('/')}}" + '/voice/' + val);
                        arr.push(audio);
                    });

                    queue_sounds(arr);
                }
            }
        };

        xhttp.open("GET", "{{URL::to('/ajax_call')}}", true);
        xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhttp.send();
    }

    call();
</script>
</html>
