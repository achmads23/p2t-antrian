@include('layouts.header-2')
    
    @php
            if(Session::get('imk')){
                $imk = Session::get('imk');
                if($imk == 'online'){
                    $imk = 'Izin Online dari P2T';
                } else if($imk == 'oss'){
                    $imk = 'OSS';
                } else if($imk == 'biasa'){
                    $imk = 'Konsul izin Biasa';
                }
            } else {
                $imk = '';
            }
        @endphp
    <section class="about-section-s5" style="padding:20px;">
            <div class="container">
                <div class="row">
                    <div class='col-xs-12' style="display: flex;flex-direction: column;justify-content: center;align-items: center;color:black !important">
                        <div style='text-align: center;'>
                            <div style="text-align:center;font-size:17px;">
                                Pelayanan Perizinan Terpadu Satu Pintu
                                <br>
                                DPMPTSP Provinsi Jawa Timur
                                <br>
                                di {{ $data->tempat_layanan->nama}}
                            </div>
                            <br>
                            <h5 class='text-center' style="font-size:17px;text-align: center">{{strtoupper($jenis_layanan) }}</h5>
                            @if ($imk!= NULL)
                                <h5 class='text-center'>{{ strtoupper($imk)}}</h5>
                            @endif
                            <h4 class='text-center'>{{ $data->sektor->nama_kategori}}</h4>
                            <h4 class='text-center'>{{ strtoupper($data->perizinan->nama_sub_kategori)}}</h4>
                            <div style="display: flex;justify-content: center;">
                                <div style="width:350px; text-align:center;font-size:85px; background-color: yellow;" class="padding-top-bottom-35">
                                    {{$data->nomor_antrian}}
                                </div>
                            </div>
                            <br>
                            <div style="text-align:center;color:black !important;font-size:17px;">
                                Sisa Antrian: <b>{{$data->jumlah_antrian-1}}</b>
                                <br>
                                Waktu: <b>{{$data->date_format}}</b>
                            </div>
                        </div>
                        <p style="margin-top: 17px;margin-bottom: 10px"> Harap menunggu sampai petugas kami memanggil, untuk informasi mengenai regulasi dapat diakses di http://p2t.jatimprov.go.id atau komputer informasi, Terimakasih</p>

                        <i><p>Halaman akan otomatis kembali dalam <span id="progressBar">5</span> detik</p></i>

                        <div class='text-center'>
                            <a href="{{url('/')}}" class="btn btn-success">Kembali ke halaman depan</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end container -->
        </section>
        <script type="text/javascript">
            var timeleft = 5;
            var downloadTimer = setInterval(function(){
              document.getElementById("progressBar").innerHTML  = timeleft;
              timeleft -= 1;
              if(timeleft <= 0){
                window.location = '{{url('/')}}'
                clearInterval(downloadTimer);
              }
            }, 1000);
        </script>
</body>

</html>