<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        margin:0;
    }

    .flex-center {
        align-items: center;
        flex-direction: column;
        display: flex;
        justify-content: center;
        height: calc(100% - 100px);
        padding:50px;
    }

    .box{
        /*border:1px solid black;*/
        padding:20px;
        margin-bottom: 25px;
        -webkit-box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
        -moz-box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
        box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
        width: 70%;
        text-align: center;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        cursor: pointer;
    }

    .text-white{
        color:white;
    }

    .green{
        background: rgb(22,162,69);
background: linear-gradient(121deg, rgba(22,162,69,1) 61%, rgba(199,245,216,1) 63%, rgba(199,245,216,1) 100%);
    }

    .box h2{
        font-size: 6vw;
        font-weight: bold;
    }

    .box > div > span{
        font-weight: bold;
        color:black;
    }

    .box > div > h3{
        margin: 0;
        font-size: 6vw;
        line-height: 5.5vw;
        font-weight: bold;
        color:black;
    }

    .m-0{
        margin:0;
    }

    .mb-0{
        margin-bottom:0;
    }

    .text-center{
        text-align: center;
    }

    #jenis-pasien{
        font-size: 2vw;
        margin:20px 0px;
    }
    #nomor-antrian{
        font-size: 10vw;
        margin: 0;
    }

    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding: 0;
        border: 1px solid #888;
        width: 40%;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {top:-300px; opacity:0} 
        to {top:0; opacity:1}
    }

    @keyframes animatetop {
        from {top:-300px; opacity:0}
        to {top:0; opacity:1}
    }

    /* The Close Button */
    .close {
        color: black;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .modal-header {
        padding: 2px 16px;
        background-color: #5cb85c;
        color: white;
    }

    .modal-body {padding: 2px 16px;}

    .header-rs{
        display: flex;
        align-items: center;
    }

    img.logo{
        margin-right: 20px;
    }

    h1.tag-antrian{
        margin-top: 0px;
    }
</style>
</head>
<body>
    <div class="flex-center height-100">
        <div class="header-rs">
            <img src="{{asset('/images/logo.png')}}" width="40px" class='logo'>
            <h3>RSU Aisyiyah Padang</h3>
        </div>
            @foreach($loket as $elem) 
                <div class="box green text-white">
                    <h2 class="m-0">LOKET {{$elem->nomor}}</h2>
                    <div>
                        <span>No Antrian</span><br>
                        <h3 id="nomor-antrian-{{$elem->id}}">-</h3>
                    </div>
                </div>  
            @endforeach
        <div class="text-center">
            <strong><span style="color:green">RSU Aisyiyah Padang</span> &copy; {{date('Y')}} </strong>
        </div>
    </div>
    <script type="text/javascript">
        function call(){
            var xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    let result = JSON.parse(xhttp.responseText);

                    result.data.forEach(function(val,index) {
                        console.log("nomor-antrian-" + val.id)
                        var nomor_antrian = document.getElementById("nomor-antrian-" + val.id);
                        nomor_antrian.innerHTML = val.antrian_saat_ini ? val.antrian_saat_ini : '-';
                    });
                }
            };

            xhttp.open("GET", "{{URL::to('/monitor/ajax')}}", true);
            xhttp.send();
        }
        call();
        setInterval(call, 5000);
    </script>
</body>
</html>
