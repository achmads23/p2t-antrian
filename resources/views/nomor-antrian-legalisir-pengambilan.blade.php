@include('layouts.header-2')  
    <section class="about-section-s5" style="padding:20px;">
            <div class="container">
                <div class="row">
                    <div class='col-xs-12 text-center' style="display: flex;flex-direction: column;justify-content: center;align-items: center;color:black">
                        <div style='width:400px;text-align: center;'>
                            <div style="text-align:center;font-size:17px;">
                                Pelayanan Perizinan Terpadu Satu Pintu
                                <br>
                                DPMPTSP Provinsi Jawa Timur
                                <br>
                                di {{ $data->tempat_layanan->nama}}
                            </div>
                            <br>
                            <b>{{$jenis_layanan == 'pengambilan' ? 'Pengambilan dan Legalisir' : 'Pendampingan OSS' }}</b>
                            <br>
                            <div style="display: flex;justify-content: center;">
                                <div style="width:350px; text-align:center;font-size:85px; background-color: yellow;" class="padding-top-bottom-35">
                                    {{$data->nomor_antrian}}
                                </div>
                            </div>
                            <div style="text-align:center;font-size:17px;margin-top:15px;">
                                Sisa Antrian: <b>{{$data->jumlah_antrian}}</b><br>
                                Waktu: <b>{{$data->date_format}}</b>
                            </div>
                        </div>
                        <p style="margin-top: 17px;margin-bottom: 10px"> Harap menunggu sampai petugas kami memanggil, Terimakasih</p>
                        <i><p>Halaman akan otomatis kembali dalam <span id="progressBar">5</span> detik</p></i>
                        <div class='text-center'>
                            <a href="{{url('/')}}" class="btn btn-success">Kembali ke halaman depan</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script type="text/javascript">
            var timeleft = 5;
            var downloadTimer = setInterval(function(){
              document.getElementById("progressBar").innerHTML  = timeleft;
              timeleft -= 1;
              if(timeleft <= 0){
                window.location = '{{url('/')}}'
                clearInterval(downloadTimer);
              }
            }, 1000);
        </script>
</body>

</html>