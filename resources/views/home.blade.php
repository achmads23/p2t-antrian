@include('layouts.header-2')

</header>
<style>
    html, body {
        height: 100%;
        overflow: hidden;
    }
    .mb-0{
        margin-bottom: 0;
    }
    .mt-0{
        margin-top: 0;
    }

    .mt-1{
        margin-top: 10px;
    }
    .mb-1{
        margin-bottom: 10px;
    }

    .nama-tempat{
        border-bottom:5px solid #3cc7bc;
        padding-bottom: 10px;
    }
    .zone-name{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
    div.text-center{
        color:black;
    }

    .zone-button{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        margin-top: 30px;
    }

    .kotak-button{
        width: 250px;
        text-align: center;
    }
    .zone-button .kotak-button .kotak {
    height: 150px;
    width: 250px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 10px;
}

    .zone-button .kotak-button .kotak.perizinan{
        background-color: #3cdbe1;
    }

    .zone-button .kotak-button .kotak.konsultasi{
        background-color: #ffc000;
    }
    .zone-button .kotak-button .kotak.pengambilan{
        background-color: #92d14f;
    }
    .zone-button .kotak-button .kotak.legalisir{
        background-color: #feccff;
    }

    .zone-button .kotak-button .kotak h2{
        color:black;   
    }
</style>
<section class="monitor-section">
    <div class="zone-name">
        <h3 class="mb-0">Selamat Datang di PTSP Provinsi Jawa Timur</h3>
        <h3 class="nama-tempat mt-1">di {{$data->tempat_layanan->nama}}</h3>
    </div>

    <div class="text-center" style="margin-top: 20px;">
        <i>Gunakan Menu Dibawah untuk Memulai</i>
    </div>

    <div class="container">
        <div class="zone-button">
            @foreach ($data->layanans as $key => $value)
                @if($value->nama == 'perizinan' && $value->value == 1)
                    <a href="{{url($value->nama)}}">
                        <div class="kotak-button">
                            <div class="kotak perizinan">
                                <h2 class="mb-0 mt-0">Perizinan</h2>
                            </div>
                            Anda melakukan pendaftaran perizinan secara langsung
                        </div>
                    </a>
                @elseif($value->nama == 'konsultasi' && $value->value == 1)
                    <a href="{{url($value->nama)}}">
                        <div class="kotak-button">
                            <div class="kotak konsultasi">
                                <h2 class="mb-0 mt-0">Konsultasi</h2>
                            </div>
                            Memberikan penjelasan menganai perijinan dan pengaduan
                        </div>
                    </a>
                @elseif($value->nama == 'pengambilan' && $value->value == 1)
                    <a href="{{url('antrian/' . $value->nama)}}">
                        <div class="kotak-button">
                            <div class="kotak pengambilan">
                                <h2 class="mb-0 mt-0">Pengambilan <br>dan<br> Legalisir</h2>
                            </div>
                            Untuk mengambil ijin yang telah anda ajukan sebelumnya
                        </div>
                    </a>
                @elseif($value->nama == 'legalisir' && $value->value == 1)
                    <a href="{{url('antrian/' . $value->nama)}}">
                        <div class="kotak-button">
                            <div class="kotak legalisir">
                                <h2 class="mb-0 mt-0">Pendampingan OSS</h2>
                            </div>
                            Guna legalisasi perijinan yang telah anda miliki
                        </div>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</section>

@include('layouts.footer')