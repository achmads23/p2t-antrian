<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/call', 'HomeController@call');
Route::get('/ajax_call', 'HomeController@store_call');
Route::post('/cek_nik', 'HomeController@cek_nik');

Route::get('/pilih_imk/{sektor?}', 'HomeController@pilih_imk');
Route::get('/antrian/{jenis?}', 'HomeController@antrian');
Route::post('/antrian/{jenis?}', 'HomeController@store_antrian');
Route::get('/{jenis?}/{id_sektor?}/{id_izin?}', 'HomeController@index');

Route::post('/ajukan', 'HomeController@store');
