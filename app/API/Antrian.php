<?php

namespace App\API;
use Config;
class Antrian{
    
    public static function index(){
        $url = Config::get('app.url_api');
        $access_key = Config::get('app.access_key');
        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $param = [];
        $result = curl_post($url,$param,0,'POST',$request_headers);
        return $result;
    }

    public static function tempat_antrian(){
        $url = Config::get('app.url_api');
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $param = [];
        $result = curl_post($url,$param,0,'POST',$request_headers);
        return $result;
    }

    public static function list_sektor(){
        $url = Config::get('app.url_api') .'/list_sektor';
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $param = [];
        $result = curl_post($url,$param,0,'POST',$request_headers);
        return $result;
    }

    public static function list_izin_sektor($id){
        $url = Config::get('app.url_api') .'/list_izin_sektor/' . $id;
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $param = [];
        $result = curl_post($url,$param,0,'POST',$request_headers);
        return $result;
    }

    public static function detail_izin($id_sektor, $id_izin){
        $url = Config::get('app.url_api') .'/detail_izin/' . $id_sektor . '/' . $id_izin;
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $param = [];
        $result = curl_post($url,$param,0,'POST',$request_headers);
        return $result;
    }

    public static function call_antrian(){
        $url = Config::get('app.url_api') . '/get_voice';
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $result = curl_post($url,[],0,'POST',$request_headers);
        return $result;
    }

    public static function ajukan($param){
        $url = Config::get('app.url_api') .'/ajukan';
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $result = curl_post($url,$param,0,'POST',$request_headers);
        return $result;
    }

    public static function antrian($param){
        $url = Config::get('app.url_api') .'/antrian';
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $result = curl_post($url,$param,0,'POST',$request_headers);
        return $result;
    }

    public static function cek_nik($nik){
        $url = Config::get('app.url_asset') .'/Monitor/API/Antrian/ajax_check';
        $access_key = Config::get('app.access_key');

        $request_headers = array();
        $request_headers[] = 'Authorization: '. $access_key;
        $result = curl_post($url,['nik' => $nik ],0,'POST',$request_headers);
        return $result;
    }
}