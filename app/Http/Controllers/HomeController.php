<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\API\Antrian as AntrianAPI;

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public $voice = [
        'A' => 'A.wav',
        'B' => 'B.wav',
        'C' => 'C.wav',
        'D' => 'D.wav',
        'E' => 'E.wav',
        'F' => 'F.wav',
        'G' => 'G.wav',
        'H' => 'H.wav',
        'I' => 'I.wav',
        'J' => 'J.wav',
        'K' => 'K.wav',
        'L' => 'L.wav',
        'M' => 'M.wav',
        'N' => 'N.wav',
        'O' => 'O.wav',
        'P' => 'P.wav',
        'Q' => 'Q.wav',
        'R' => 'R.wav',
        'S' => 'S.wav',
        'T' => 'T.wav',
        'U' => 'U.wav',
        'V' => 'V.wav',
        'W' => 'W.wav',
        'X' => 'X.wav',
        'Y' => 'Y.wav',
        'Z' => 'Z.wav',
        'satu' => 'SATU.wav',
        'dua' => 'DUA.wav',
        'tiga' => 'TIGA.wav',
        'empat' => 'EMPAT.wav',
        'lima' => 'LIMA.wav',
        'enam' => 'ENAM.wav',
        'tujuh' => 'TUJUH.wav',
        'delapan' => 'DELAPAN.wav',
        'sembilan' => 'SEMBILAN.wav',
        'sepuluh' => 'SEPULUH.wav',
        'sebelas' => 'SEBELAS.wav',
        'belas' => 'BELAS.wav',
        'ribu' => 'RIBU.wav',
        'ratus' => 'RATUS.wav',
        'puluh' => 'PULUH.wav',
    ];

    function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
    }

    public function pilih_imk($imk){
        session(['imk' => $imk]);
        return redirect('konsultasi');
    }

    public function index(Request $request, $jenis = false,$id_sektor = false, $id_izin = false)
    {
    	$access_key = Config::get('app.access_key');
        $url_api = Config::get('app.url_api');
    	if(!$access_key && !$url_api){
    		return view('no-key');	
    	} 
        if($jenis != false && $id_sektor != false && $id_izin != false) {
            $result = AntrianAPI::detail_izin($id_sektor, $id_izin);
            if($result){
                return view('lengkapi-data', ['data' => $result->data,'jenis_layanan' => $jenis]);
            } else {
                header("Refresh:0; url=" . url('/'));
            }

        } else if($jenis != false && $id_sektor != false) {
            $result = AntrianAPI::list_izin_sektor($id_sektor);
            if($result){
                return view('pilih-izin', ['data' => $result->data,'jenis_layanan' => $jenis]);
            } else {
                header("Refresh:0; url=" . url('/'));
            }

        } else if($jenis != false){
            if($jenis == 'konsultasi' && $request->session()->get('imk') == false){
                $result = AntrianAPI::tempat_antrian();
                if($result){
                    return view('pilih-imk', ['data' => $result->data,'jenis_layanan' => $jenis]);
                } else {
                    header("Refresh:0; url=" . url('/'));
                }

            } else {
                $result = AntrianAPI::list_sektor();
                if($result){
                    return view('pilih-sektor', ['data' => $result->data,'jenis_layanan' => $jenis]);
                } else {
                    header("Refresh:0; url=" . url('/'));
                }
            }
        } else {
            $request->session()->forget('imk');
            $result = AntrianAPI::index();
            if($result){
                return view('home', ['data' => $result->data]);
            } else {
                header("Refresh:0");
            }
        }
    }

    public function call()
    {
        $access_key = Config::get('app.access_key');
        $url_api = Config::get('app.url_api');
        if(!$access_key && !$url_api){
            return view('no-key');
        }
        return view('call');
    }

    public function store_call()
    {
        $result = AntrianAPI::call_antrian();
        if(isset($result) && $result != [] && $result->data){

            $nomor = explode(' ', $result->data->antrian);
            $huruf = $nomor[0];
            $terbilang = $this->penyebut((int)$nomor[1]);
            $terbilang = explode(' ',$terbilang);

            $array = [];
            array_push($array, 'DINGDONG.wav');
            array_push($array, 'NOMORANTRIAN.wav');
            array_push($array, $this->voice[$huruf]);
            foreach ($terbilang as $key => $value) {
                if($value){
                    array_push($array, $this->voice[$value]);
                }
            }
            
            if($result->data->tipe == 'meja')
                array_push($array, 'MEJA.wav');
            else if($result->data->tipe == 'ruang%20informasi')
                array_push($array, 'DIRUANGINFORMASIMEJA.wav');
            else 
                array_push($array, 'LOKET.wav');

            $terbilang = $this->penyebut((int)$result->data->nama_meja);
            $terbilang = explode(' ',$terbilang);
            foreach ($terbilang as $key => $value) {
                if($value){
                    array_push($array, $this->voice[$value]);
                }
            }

            $result->data->array_panggilan = $array;
            return response()->json([
                        'code' => 200,
                        'data' => $result->data,
                        'msg' => 'Sukses',
                    ], 200);
        } else {
            return response()->json([
                        'code' => 400,
                        'data' => [],
                        'msg' => 'Sukses',
                    ], 200);
        }
    }

    function print($jenis, $lokasi, $nomor_antrian, $date,$sisa_antrian, $barcode = false){
        $connector = new WindowsPrintConnector(Config::get('app.print_name'));
        $printer = new Printer($connector);

        $logo = EscposImage::load(public_path() . "/images/logo-jatim-kecil.png", false);
        $printer -> initialize();
        
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> bitImage($logo,3);
        $printer -> text("\n");
        /* Print top logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text("Pelayanan Perizinan Terpadu Satu Pintu\n");
        $printer -> text("DPMPTSP Provinsi Jawa Timur\n");
        $printer -> text($lokasi->nama . "\n");
        $printer -> text("\n");
        $printer -> text("Nomor Antrian Anda\n");
        $printer -> text("\n");
        $printer -> text($jenis . "\n");
        $printer->setTextSize(4,4);
        $printer -> text("\n");
        $printer->text($nomor_antrian);
        $printer -> text("\n");
        $printer -> text("\n");
        /* Name of shop */
        $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        /* Footer */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer->setTextSize(1,1);
        $printer -> text("Sisa Antrian: " . $sisa_antrian . "\n");
        $printer -> text("Waktu kedatangan\n");
        $printer -> text($date . "\n");
        $printer -> text("\n");
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        if($barcode) {
            $pieces = explode("/",$barcode);
            $img = public_path('/barcode/'. $pieces[count($pieces)-1]);
            file_put_contents($img, file_get_contents($barcode));

            $barcode = EscposImage::load($img, false);
            $printer -> bitImage($barcode,3);
        }
        $printer -> pulse();
        $printer -> cut();
        
        $printer -> close();

        return true;
    }

    public function store(Request $request){
        $param = $request->input();
        $result = AntrianAPI::ajukan($param);
        if($result){
            $this->print($param['jenis_layanan'], $result->tempat_layanan, $result->nomor_antrian, $result->date_format,$result->jumlah_antrian-1, $result->barcode);
            return view('nomor-antrian', ['data' => $result,'jenis_layanan' => $param['jenis_layanan']]);
        } else {
            header("Refresh:0; url=" . $request->url);
        }
    }

    public function antrian($jenis){
        $access_key = Config::get('app.access_key');
        $url_api = Config::get('app.url_api');
        if(!$access_key && !$url_api){
            return view('no-key');
        }

        $result = AntrianAPI::tempat_antrian();
        return view('antrian',['jenis_layanan' => $jenis, 'data' => $result->data]);

        $result = AntrianAPI::antrian($jenis);
        if($result){
            $jenis = $jenis == 'pengambilan' ? 'Pengambilan dan Legalisir' : 'Pendampingan OSS';
            $this->print($jenis, $result->tempat_layanan, $result->nomor_antrian, $result->date_format,$result->jumlah_antrian);
            return view('nomor-antrian-legalisir-pengambilan', ['data' => $result,'jenis_layanan' => $jenis]);
        } else {
            header("Refresh:0;");
        }
    }

    public function store_antrian(Request $request, $jenis){
        $param = $request->input();
        $result = AntrianAPI::antrian($param);
        if($result){
            $jenis = $jenis == 'pengambilan' ? 'Pengambilan dan Legalisir' : 'Pendampingan OSS';
            $this->print($jenis, $result->tempat_layanan, $result->nomor_antrian, $result->date_format,$result->jumlah_antrian, $result->barcode);
            return view('nomor-antrian-legalisir-pengambilan', ['data' => $result,'jenis_layanan' => $jenis]);
        } else {
            header("Refresh:0; url=" . $request->url);
        }
    }

    public function cek_nik(Request $request){
        $result = AntrianAPI::cek_nik($request->post('nik'));
        return response()->json([
                    'code' => 200,
                    'data' => $result,
                    'msg' => 'Sukses',
                ], 200);
    }
}
