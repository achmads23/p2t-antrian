<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
// use Session;

//use Request;


function curl_post($url, $param, $is_json = 0, $method = 'POST',$headers=[]){
	$ch = curl_init();    
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , 0);
    curl_setopt($ch, CURLOPT_USERAGENT, "User-Agent: Some-Agent/1.0");

    if($is_json){
        curl_setopt($ch, CURLOPT_POSTFIELDS, 
            json_encode($param)
        );
    } else {
        curl_setopt($ch, CURLOPT_POSTFIELDS, 
            http_build_query($param)
        );
    }
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


    $server_output = curl_exec($ch);
    $http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
    $return = json_decode($server_output);  
    curl_close ($ch);

    if($return !== false) {
        return $return;
    } else {
        return $return = false;
    }
}

function rename_day($day){
    switch ($day) {
        case 'Sunday':
            return 'Minggu';
            break;
        case 'Monday':
            return 'Senin';
            break;
        case 'Tuesday':
            return 'Selasa';
            break;
        case 'Wednesday':
            return 'Rabu';
            break;
        case 'Thursday':
            return 'Kamis';
            break;
        case 'Friday':
            return 'Jumat';
            break;
        case 'Saturday':
            return 'Sabtu';
            break;
    }
}

function multiexplode ($delimiters,$string) {

    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  $launch;
}